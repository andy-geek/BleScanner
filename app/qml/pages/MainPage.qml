// SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.SimpleBLE 1.0

Page {
    objectName: "mainPage"       
    allowedOrientations: Orientation.All

    SilicaListView {
        id: deviceListView

        anchors.fill: parent
        model: scanModel
        spacing: Theme.paddingLarge
        delegate: ListItem {
            menu: ContextMenu {

                MenuItem {
                    text: connected ? qsTr("Disconnect") : qsTr("Connect")
                    onClicked: connected ? scanModel.disconnectFromDevice(index) : scanModel.connectToDevice(index)
                }

                MenuItem {
                   enabled: connected
                   text: qsTr("Send command")
                   onClicked: {
                       pageStack.push(Qt.resolvedUrl("CommandPage.qml"),
                                      {index: index, scanModel: scanModel})
                   }
                }

                MenuItem {
                    enabled: connected
                    text: qsTr("Introspect")
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("AboutDevice.qml"),
                                       {serviceList: scanModel.introspectService(index)})
                    }
                }
            }

            Column {
                width: parent.width / 2
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingMedium
                }
                Label {
                    text: name === "" ? address : name + " - " + address
                }
                Label {
                    text: connected ? qsTr("connected") : qsTr("not connected")
                    font.pixelSize: Theme.fontSizeExtraSmall
                    color: Theme.secondaryHighlightColor
                }
                Label {
                    text: paired ? qsTr("paired") : qsTr("not paired")
                    font.pixelSize: Theme.fontSizeExtraSmall
                    color: Theme.secondaryHighlightColor
                }
            }
            Label {
                text: rssi
                anchors {
                    right: parent.right
                    rightMargin: Theme.paddingMedium
                    verticalCenter: parent.verticalCenter
                }
            }
        }
        header: ListItem {
            Row {
                height: parent.height
                spacing: Theme.paddingMedium
                anchors.horizontalCenter: parent.horizontalCenter

                Label {
                    text: scanModel.discovering ? qsTr("scan in process") : qsTr("pull down find devices")
                    width: implicitWidth
                    color: Theme.highlightColor
                    anchors.verticalCenter: parent.verticalCenter
                }
                BusyIndicator {
                    running: scanModel.discovering
                    visible: running
                    size: BusyIndicatorSize.Small
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }

        PullDownMenu {
            MenuItem {
                onClicked: scanModel.discovering = !scanModel.discovering
                text: scanModel.discovering ? qsTr("Stop scan") : qsTr("Start scan")
            }
            MenuItem {
                enabled: deviceListView.count !== 0
                text: qsTr("Clean list")
                onClicked: scanModel.clear()
            }
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        }
    }

    Label {
        id: poweredLabel

        anchors.centerIn: parent
        visible: deviceListView.count === 0
        text: scanModel.powered ? qsTr("Powered On") : qsTr("Powered Off")
    }

    ScanModel {
        id: scanModel
    }
}
