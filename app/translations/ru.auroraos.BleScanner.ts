<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutDevice</name>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="20"/>
        <source>Services list</source>
        <translatorcomment>Services list</translatorcomment>
        <translation>Services list</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="41"/>
        <source>UUID: </source>
        <translatorcomment>UUID: </translatorcomment>
        <translation>UUID: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="53"/>
        <source>Valid: </source>
        <translatorcomment>Valid: </translatorcomment>
        <translation>Valid: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="67"/>
        <source>Path: </source>
        <translatorcomment>Path: </translatorcomment>
        <translation>Path: </translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Short description of my Aurora OS Application&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2023 ru.auroraos&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
        &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
        &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
        &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>AboutService</name>
    <message>
        <location filename="../qml/pages/AboutService.qml" line="21"/>
        <source>Characteristic list</source>
        <translatorcomment>Characteristic list</translatorcomment>
        <translation>Characteristic list</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutService.qml" line="37"/>
        <source>UUID: </source>
        <translatorcomment>UUID: </translatorcomment>
        <translation>UUID: </translation>
    </message>
</context>
<context>
    <name>CommandPage</name>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="17"/>
        <source>Terminal</source>
        <translatorcomment>Terminal</translatorcomment>
        <translation>Terminal</translation>
    </message>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="23"/>
        <source>Enter command:</source>
        <translatorcomment>Enter command:</translatorcomment>
        <translation>Enter command:</translation>
    </message>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="27"/>
        <source>Send</source>
        <translatorcomment>Send</translatorcomment>
        <translation>Send</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>BLE Scanner</source>
        <translatorcomment>BLE Scanner</translatorcomment>
        <translation>BLE Scanner</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="22"/>
        <source>Connect</source>
        <translatorcomment>Connect</translatorcomment>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="22"/>
        <source>Disconnect</source>
        <translatorcomment>Disconnect</translatorcomment>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Send command</source>
        <translatorcomment>Send command</translatorcomment>
        <translation>Send command</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="37"/>
        <source>Introspect</source>
        <translatorcomment>Introspect</translatorcomment>
        <translation>Introspect</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>connected</source>
        <translatorcomment>connected</translatorcomment>
        <translation>connected</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>not connected</source>
        <translatorcomment>not connected</translatorcomment>
        <translation>not connected</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="60"/>
        <source>paired</source>
        <translatorcomment>paired</translatorcomment>
        <translation>paired</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="60"/>
        <source>not paired</source>
        <translatorcomment>not paired</translatorcomment>
        <translation>not paired</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <source>scan in process</source>
        <translatorcomment>scan in process</translatorcomment>
        <translation>scan in process</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <source>pull down find devices</source>
        <translatorcomment>pull down find devices</translatorcomment>
        <translation>pull down find devices</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="98"/>
        <source>Stop scan</source>
        <translatorcomment>Stop scan</translatorcomment>
        <translation>Stop scan</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="98"/>
        <source>Start scan</source>
        <translatorcomment>Start scan</translatorcomment>
        <translation>Start scan</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>Clean list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>Powered On</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>Powered Off</source>
        <translation></translation>
    </message>
</context>
</TS>
