import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    property var index
    property var scanModel

    objectName: "commandPage"
    allowedOrientations: Orientation.All

    Column {
        anchors.fill: parent
        width: parent.width
        spacing: Theme.paddingMedium

        PageHeader {
            title: qsTr("Terminal")
        }

        TextField {
            id: textField
            width: parent.width
            placeholderText: qsTr("Enter command:")
        }

        Button {
            text: qsTr("Send")

            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: {
                scanModel.sendCommand(index, qsTr(textField.text))
            }
        }
    }
}
