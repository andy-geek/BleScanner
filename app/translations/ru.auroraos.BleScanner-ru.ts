<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutDevice</name>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="20"/>
        <source>Services list</source>
        <translatorcomment>Список сервисов</translatorcomment>
        <translation>Список сервисов</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="41"/>
        <source>UUID: </source>
        <translatorcomment>UUID: </translatorcomment>
        <translation>UUID: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="53"/>
        <source>Valid: </source>
        <translatorcomment>Действующий: </translatorcomment>
        <translation>Действующий: </translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutDevice.qml" line="67"/>
        <source>Path: </source>
        <translatorcomment>Путь: </translatorcomment>
        <translation>Путь: </translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="23"/>
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="33"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Short description of my Aurora OS Application&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="38"/>
        <source>3-Clause BSD License</source>
        <translation>Лицензия 3-Clause BSD</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="48"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2023 ru.auroraos&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
    &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
    &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
    &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>AboutService</name>
    <message>
        <location filename="../qml/pages/AboutService.qml" line="21"/>
        <source>Characteristic list</source>
        <translatorcomment>Список характеристик</translatorcomment>
        <translation>Список характеристик</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutService.qml" line="37"/>
        <source>UUID: </source>
        <translatorcomment>UUID: </translatorcomment>
        <translation>UUID: </translation>
    </message>
</context>
<context>
    <name>CommandPage</name>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="17"/>
        <source>Terminal</source>
        <translatorcomment>Терминал</translatorcomment>
        <translation>Терминал</translation>
    </message>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="23"/>
        <source>Enter command:</source>
        <translatorcomment>Введите команду:</translatorcomment>
        <translation>Введите команду:</translation>
    </message>
    <message>
        <location filename="../qml/pages/CommandPage.qml" line="27"/>
        <source>Send</source>
        <translatorcomment>Отправить</translatorcomment>
        <translation>Отправить</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="12"/>
        <source>BLE Scanner</source>
        <translatorcomment>BLE Сканер</translatorcomment>
        <translation>BLE Сканер</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="22"/>
        <source>Connect</source>
        <translatorcomment>Подключить</translatorcomment>
        <translation>Подключить</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="22"/>
        <source>Disconnect</source>
        <translatorcomment>Отключить</translatorcomment>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="28"/>
        <source>Send command</source>
        <translatorcomment>Отправить команду</translatorcomment>
        <translation>Отправить команду</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="37"/>
        <source>Introspect</source>
        <translatorcomment>Смотреть содержимое</translatorcomment>
        <translation>Смотреть содержимое</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>connected</source>
        <translatorcomment>подключен</translatorcomment>
        <translation>подключен</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="55"/>
        <source>not connected</source>
        <translatorcomment>не подключен</translatorcomment>
        <translation>не подключен</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="60"/>
        <source>paired</source>
        <translatorcomment>сопряжен</translatorcomment>
        <translation>сопряжен</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="60"/>
        <source>not paired</source>
        <translatorcomment>не сопряжен</translatorcomment>
        <translation>не сопряжен</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <source>scan in process</source>
        <translatorcomment>процесс сканирования</translatorcomment>
        <translation>процесс сканирования</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <source>pull down find devices</source>
        <translatorcomment>потяните вниз, чтобы найти устройство</translatorcomment>
        <translation>потяните вниз, чтобы найти устройство</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="98"/>
        <source>Stop scan</source>
        <translatorcomment>Остановить сканирование</translatorcomment>
        <translation>Остановить сканирование</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="98"/>
        <source>Start scan</source>
        <translatorcomment>Запустить сканирование</translatorcomment>
        <translation>Запустить сканирование</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="102"/>
        <source>Clean list</source>
        <translatorcomment>Очистить список</translatorcomment>
        <translation>Очистить список</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>About</source>
        <translatorcomment>О приложении</translatorcomment>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>Powered On</source>
        <translatorcomment>Питание включено</translatorcomment>
        <translation>Питание включено</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>Powered Off</source>
        <translatorcomment>Питание выключено</translatorcomment>
        <translation>Питание выключено</translation>
    </message>
</context>
</TS>
