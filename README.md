# BLE Scanner

The project provides an example of using SimpleBLE library to scaning BLE devices.

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.

* **[ru.auroraos.BleScanner.pro](ru.auroraos.BleScanner.pro)** file
  describes the subdirs project that contains two two subprojects where the first is responsible
  for building the SimpleBLE library, and the second describes the structure of the application for
  the qmake build system.
* **[icons](app/icons)** directory contains application icons for different screen resolutions.
* **[qml](app/qml)** directory contains the QML source code and the UI resources.
  * **[cover](app/qml/cover)** directory contains the application cover implementations.
  * **[icons](app/qml/icons)** directory contains the custom UI icons.
  * **[pages](app/qml/pages)** directory contains the application pages.
  * **[BleScanner.qml](app/qml/BleScanner.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  **[ru.auroraos.BleScanner.spec](rpm/ru.auroraos.BleScanner.spec)** file is used by rpmbuild tool.
* **[src](app/src)** directory contains the C++ source code.
  * **[main.cpp](app/src/main.cpp)** file is the application entry point.
* **[translations](app/translations)** directory contains the UI translation files.
* **[ru.auroraos.BleScanner.desktop](app/ru.auroraos.BleScanner.desktop)** file
  defines the display and parameters for launching the application.

## Compiling this application

To compile the application on the SDK, you need to install the following
packages inside the development environment:  
- git
- git-core

