# Authors

* Evgeniy Samohin, <e.samohin@omp.ru>
  * Developer, Maintainer 2023

# Reviewers

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2023
  * Reviewer, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer, 2023
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
